#ifndef OBR_H
#define OBR_H 

extern int ShowTimes;

/*
 * EO ružová - pwm
 * A hnědá - group select
 * gnd
 * B červená - group select
 * gnd
 * gnd
 * gnd
 * clk modrá
 * gnd
 * SCLK šedá
 * gnd 
 * R zelená
 * 4x gnd 
 */
#define EO 11      //ruzova
#define EOval 50
#define A 7      //hneda
#define B 6      //červena
#define STORE 5  //šeda

//pokud to neni SPI
#define SHIFT 52  //CLK - modra
#define DATA 51   //MOSI - zelena

#define VYCHOZI_RYCHLOST 5

#include <Arduino.h>
#include <SPI.h>
#include "Pismeno.h"


class Ledky{
private:
  public:
  
  //Pismeno abeceda[95];
  unsigned char pocet, rychlost = VYCHOZI_RYCHLOST;
  uint32_t** obraz;       //ukazatel na první řádek (uplně celý řádek)
  Ledky(unsigned char n);


  void shift(uint16_t sloupec); //posune pole a pripoji sloupec na zacatek
  void shift(); //prazdny sloupec
    //void shift(uint32_t * radek); //posune pole (a na kraji nechá nuly)

  void vypis(unsigned char n);

  //dá hodnoty do zařízení, uloží a ukáže 1x - ne celou dobu písmena
  void Show();

    //zobrazí potřebnou délku
    void zobraz();

  void napis(uint32_t letter, bool show);

  void napis(String string, bool show);


  void Clear(bool show = false);

  void info(String text);
};

#endif 

