Arduino projekt pro ledky P10
==========

SD karta
----------
**Formát:** FAT32

**Maxiální délka názvů souborů:** 12 znaků vč. přípony .txt

**Adresářová struktura:**

```bash
/
├── playlist
│   ├── play1.txt
│   └── play2.txt
└── song
    ├── song1.TXT
    └── song2.txt
```

**Formát souborů:** UTF-8 BE, typ konce řádku může být libovolný

**Struktura playlistů:**

```bash
song1\n
song2\n
```

**Struktura písní:**
```bash
3\n                         # rychlost přehrávání; rozsah 1-9; vyšší = rychlejší
100:Text písničky\n         # toto se přehraje v 10. sekundě
250:Pokračování textu\n     # toto se přehraje ve 25. sekundě
300:Konec písně\n
```

Použití
-------

Výchozí mód je přehrávání podle playlistu. 
Po zapnutí se vypíše *Vyberte playlist:* a zobrazí se první playlist. 
Zmáčknutím tlačítka se přesune na další playlist. Podržením tlačítka (alespoň 3 sek.) se playlist vybere.
Hned, jak název playlistu odjede z panelů je zařízení připraveno. Po zmáčknutí se hned začne píseň přehrávat.
Po posledním řádku písně zařízení čeká na další zmáčknutí, kterým se spustí další píseň.

Dalším módem je přímý výběr písničky. Pro spuštění držte tlačítko a zmáčkněte reset (červené tlačítko).
Tlačítko puste, až se zobrazí *Vyberte song*. Výběr funguje stejně jako výběr plalistu. Potom, co 
název písně odjede, zařízení čeká na další zmáčknutí pro spuštění. 

Debug
-----

Zařízení vysílá debug informace přes sériový port s rychlostí 9600 baudů.

Projekt
-------
Odkaz na zdrojové kóddy:
```bash
git clone https://Bagnat@bitbucket.org/Bagnat/arduino-ledky.git
```