#ifndef INTERFACE_H
#define INTERFACE_H

#include <Arduino.h>
#include "Ledky.h"
#include "Soubor.h"
#include <SPI.h>
#include <SD.h>

extern Ledky* ledky;

#define BUTTON 2

#define TRIGGER 26  //neni
#define CERVENA 28  //neni
#define ZELENA 30   //neni
#define SelectorSD 4




class Interface{
public:

    //File playlist;
    uint16_t cislo_pisne, cislo_playlistu; //cislo pisne je jednou číslo v playlistu a jednou v adresáři!


    Interface();

    void vyberPlaylist();
    String getPlaylistName();
    void vyberSong();
    String getSongNameByPlaylist();
    String getSongNameByNumber();

    void prehrajSong( Soubor &pisen );
    uint16_t getTime(Soubor &radek);

    static uint8_t delka_zmacknuti(bool show); ///< ceka na zmacknuti a pak vrati delku zmacknuti v desetinách sekundy = 10 = sekunda
    static bool zmacknuti();

    

};

#endif
