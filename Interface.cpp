#include "Interface.h"


Interface::Interface(){
  pinMode(INPUT_PULLUP, BUTTON);
}

//Prace s playlistem
void Interface::vyberPlaylist(){
    String dir = "/playlist/";
    Soubor::vyberSoubor( dir, cislo_playlistu);
    cislo_pisne = 0;
};

String Interface::getPlaylistName(){
    String dir = "/playlist/";
    return Soubor::getSouborName( dir, cislo_playlistu);
}

void Interface::vyberSong(){
    String dir = "/song/";
    Soubor::vyberSoubor( dir, cislo_pisne);
};


//Prace se songem
String Interface::getSongNameByPlaylist(){

    Soubor playlist( "/playlist/" + getPlaylistName() /*+ ".txt"*/ );
    String name;

    //preskoceni prvnich pisni
    for(int i = 0 ; i<cislo_pisne; i++){
        playlist.getLineASCII();
    }

    name = playlist.getLineASCII();
    return name;
}

String Interface::getSongNameByNumber(){
    String dir = "/song/";
    return Soubor::getSouborName( dir, cislo_pisne );
}

void Interface::prehrajSong( Soubor &pisen )
{

    unsigned long cas = millis();

    uint16_t rychlost = pisen.getZnak();
    if(rychlost == 0) return;
    ledky->rychlost = 10-(rychlost-'0');
    pisen.getZnak(); //konec řádku

    //iterace pisne
    while(true){
        //radek
        uint32_t zpozdeni = getTime(pisen);
        if(zpozdeni == 0) break;
Serial.println(zpozdeni);
        //cekani
        ledky->napis(' ', true);
        while ( (cas + (zpozdeni*100) ) > millis() ) ledky->napis(' ', true);

        uint16_t znak;
        while(true){
            znak = pisen.getZnak();
            //Serial.print("znak: "); Serial.println(znak);
            if(znak == 0) break;
            ledky->napis(znak, true);
        }

    }

    ledky->Clear(true);
    ledky->rychlost = VYCHOZI_RYCHLOST;
}

uint16_t Interface::getTime(Soubor &radek){
    uint16_t time = 0;
    char znak;

    while(true){
        znak = (char) radek.getZnak();
        //Serial.print("time: "); Serial.println(znak);
        if(znak == ':' || znak == 0) return time;
        time *= 10;
        time += znak - '0';
    }

}




uint8_t Interface::delka_zmacknuti(bool show) {
    delay(100);
    while(!zmacknuti()) if(show) ledky->zobraz();
    unsigned long mil = millis();
    delay(100);
    while(zmacknuti()) if(show) ledky->zobraz();
    return (millis() - mil)/100;
}

bool Interface::zmacknuti() {
    for (int i = 0; i < 3001; ++i) {
        if(digitalRead(BUTTON) != HIGH) return false;
    }
    return true;
}


