//
// Created by davidpokzgmail.com on 4/13/2019.
//

#ifndef LEDKY_SOUBOR_H
#define LEDKY_SOUBOR_H

#define MSB true

#include <Arduino.h>
#include <SD.h>

#if MSB == false
enum automaton_LSB{slichy, ssudy, speek};
#endif

class Soubor {
private:
    File file;

    #if MSB == false
    automaton_LSB stav = slichy;
    char tmp_var;
    #endif

    char getByteAutomaton(bool peek = false);
public:
    Soubor(String &path);
    ~Soubor();
    uint16_t getZnak();
    String getLineASCII();

    static void vyberSoubor(const String &path, uint16_t &poradi);
    static String getSouborName(const String &path, const uint16_t &poradi);
};


#include "Ledky.h"
extern Ledky* ledky;
#include "Interface.h"


#endif //LEDKY_SOUBOR_H
