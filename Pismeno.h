#ifndef PISMENO_H
#define PISMENO_H

#include <Arduino.h>
#include <SD.h>
#include "Ledky.h"
#include "utf8/arial.h"




class Pismeno{
private:

    uint32_t index;
    uint16_t data[16];
    uint8_t iteratorSloupce = 0;
public:
    uint8_t width;
    Pismeno(uint32_t unicode);

    ~Pismeno();

    /**
     * Vrátí i-ty sloupec pismene
     * @param i
     * @return
     */
    uint16_t ZpracujData( uint8_t );
};



#endif
