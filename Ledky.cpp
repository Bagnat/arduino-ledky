#include "Ledky.h"

#define SERIAL_PRINT true

    Ledky::Ledky(unsigned char n) : pocet(n) {

        pinMode(EO, OUTPUT);
        pinMode(A, OUTPUT);
        pinMode(B, OUTPUT);
        pinMode(SHIFT, OUTPUT);
        pinMode(STORE, OUTPUT);
        pinMode(DATA, OUTPUT);
        analogWrite(EO, EOval);
        digitalWrite(STORE, LOW);

        obraz = (uint32_t **) malloc( 32 ); // sizeof(uint32_t *)*16

        //16 * pocet(5) * 4 = 320
        for(uint8_t i = 0; i<16; i++)
            obraz[i] = (uint32_t *) calloc(pocet, 4); // sizeof(uint32_t) = 4


    }



    void Ledky::shift(uint16_t sloupec){
        for(uint8_t i = 0; i <16; i++) { //pro každý řádek
           for(uint8_t j = pocet-1; j != 0; j--){ //pro každý panel
             obraz[i][j] = (obraz[i][j] >> 1) | ((obraz[i][j-1]&1)<<31);
           }
           obraz[i][0] >>= 1;
        }

        unsigned char ctr = 0;
        for(uint16_t i = 0x8000; i !=0 ; i>>=1,ctr++) {
            if( sloupec & i ) {
              obraz[ctr][0] |= 0x80000000;
            }
        }
    }

    void Ledky::shift(){
        this->shift((uint16_t)0xffff);
    }


  void Ledky::vypis(unsigned char n){ // n je 0-3 //zatím jen pro panel 1
    unsigned char tmp, radek;
    char screen, sloupec;

    for(screen = pocet-1; screen >= 0; screen--){
      for(sloupec = 0; sloupec < 32; sloupec+=8)
      {
          for(radek = n; radek <16; radek += 4)
          {
              tmp = (obraz[radek][screen] >> (sloupec) );
              SPI.transfer(tmp);
          }
      }
    }

    digitalWrite(A, (n&1) ? LOW : HIGH);
    digitalWrite(B, (n&2) ? LOW : HIGH);

    digitalWrite(STORE, HIGH);
    digitalWrite(STORE, LOW);

  }

  
  void Ledky::Show(){
    SPI.setBitOrder(LSBFIRST);  //todo: lze zmenit již ve výpočtu pismene
    vypis(0);
    vypis(1);
    vypis(3);
    vypis(2);
    SPI.setBitOrder(MSBFIRST);
      //if(ShowTimes < -1 ) Serial.println(ShowTimes);
  }

  void Ledky::napis(uint32_t letter,  bool show){
     Pismeno pismeno = Pismeno( letter ); //10-13ms

     for(int i = 0; i < pismeno.width; i++){
         uint16_t sloupec = pismeno.ZpracujData(i);  //prevazne 0ms občas i 2ms
         this->shift(sloupec);                            //prevazne 0ms občas i 2ms
         if(show) zobraz();                         //40ms při ShowTimesNUMBER 10, 74 ms ShowTimesNUMBER 20
     }

      shift();
      if(show) zobraz();
  }

  void Ledky::napis(String string, bool show){
     for(auto i = string.begin(); i < string.end(); i++){
        napis( (unsigned char)*i, show);
     }
  }

  void Ledky::zobraz(){
      for(int i = 0; i < rychlost ; i++){
          Show();
      }
  }
  


void Ledky::Clear(bool show) {
    for (int i = 0; i < pocet*4*8; ++i) {
        shift();
        if(show) zobraz();
    }
}

void Ledky::info(String text){
   this->Clear();
   this->napis(text, false);
   for (int i = 0; i < 60; ++i) this->zobraz();
};
