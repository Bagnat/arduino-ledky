#include "Pismeno.h"
#include "utf8/unicode.h"

extern Ledky* ledky;

Pismeno::Pismeno(uint32_t unicode) {
   // Serial.println(sizeof(lv_font_glyph_dsc_t.w_px), 16);
    const void* ukazatel =  &(unicode_glyph_dsc[ unicode - unicode_offset ]);
    width = pgm_read_byte( ukazatel );
    index = pgm_read_byte( ukazatel+3 )<<16 |  pgm_read_word( ukazatel+1 );

    //pocitam max s width = 16
    uint8_t posun = width / 8;
    if(posun*8 != width) posun++;
    for (int i = 0; i < 16; i++) {
        data[ i ] = pgm_read_word(  &(unicode_glyph_bitmap[ index + i*posun ]) );
        data[ i ] = ~(data[ i ]<<8 | data[ i ]>>8);
    }
}

uint16_t Pismeno::ZpracujData(uint8_t n) {
    uint16_t sloupec = 0;
    uint16_t maska = 0x8000 >> n;
    for (int i = 15; i >= 0; --i) {
        sloupec<<=1;
        if(maska & data[i]) sloupec++;
    }
    return sloupec;
}

Pismeno::~Pismeno() {
}


