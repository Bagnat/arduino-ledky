//
// Created by davidpokzgmail.com on 4/13/2019.
//

#include "Soubor.h"
#include "memory.h"
//musí být utf-16 LE!!
Soubor::Soubor(String &path){
    file = SD.open( path );
    if(!file) {
        Serial.println("!f");
        Serial.println(path);
        Serial.println( freeMemory() );
    }

    //zbavit se prvnách 2B co zančí typ UTF-8
    //getZnak();
}

Soubor::~Soubor(){
    file.close();
}

String Soubor::getLineASCII(){
    String line;
    char znak;
    while (true){
        znak = (char) getZnak();
        if(znak == 0) break;
        line += znak;
    }
    return line;
}



//dalsi znak unicode, pokud je konec řádku, odstraní ho a vrátí 0
uint16_t Soubor::getZnak(){
    uint16_t znak;

    while(getByteAutomaton(true) == 0) {
        getByteAutomaton();
    }

    char byte = getByteAutomaton();
    if (byte != '\n' && byte != '\r' && byte != -1)
    {
        //ziskani utf znaku
        if((0x80 & byte) == 0) {
            return (uint16_t) byte;
        }
        else if((0xE0 & byte) == 0xC0){ //pokud začíná na "110"
            znak = ((byte & 0x1F) << 6) | getByteAutomaton() & 0x3F;
            return znak;
        }/*else{
            znak = byte & 0xF;
            znak <<= 6;
            znak |= getByteAutomaton() & 0x3F;
            znak <<= 6;
            znak |= getByteAutomaton() & 0x3F;
            return znak;
        }*/
    }
    while(getByteAutomaton(true) == '\n' || getByteAutomaton(true) == '\r') getByteAutomaton();
    return (uint16_t) 0;
}

#if MSB == false
/*
 * O toto se stará konečný automat kvuli LSB
 */
char Soubor::getByteAutomaton(bool peek){
    char ret;

    //store
    if(stav == slichy) tmp_var = file.read();

    //return
    if(stav == ssudy) ret = tmp_var;
    else if(!peek) ret = file.read();
    else ret = file.peek();

    //stav change
    if(stav == ssudy) stav = peek ? ssudy : slichy;
    else stav = peek ? speek : ssudy;
    if(!peek){
        Serial.print("byte: ");
        Serial.println(ret, 16);
        Serial.println(ret);
    }
    return ret;
}

#else

char Soubor::getByteAutomaton(bool peek){
    if(peek) return file.peek();
    return file.read();
}

#endif

/*
 * pro UTF16 LE
 * @return

uint16_t Soubor::getZnak(){
    uint16_t znak;
    znak = file.read() | file.read()<<8;
    if(znak == 0x000a || znak== 0x000d) return (uint16_t) 0;
    return znak;
}
*/



//Prace s playlistem
void Soubor::vyberSoubor(const String &path, uint16_t &poradi){
    poradi = 0;
    do{
        label_vyber:
        String name = getSouborName(path, poradi);
        if(name == ""){
            poradi = 0;
            goto label_vyber;
        }
        ledky->Clear();
        ledky->napis(name, false);
        poradi++;
    }while(Interface::delka_zmacknuti(true) < 20);

    poradi--; // o jeden to prejede
    ledky->Clear(true);
};

/**
 *
 * @param path string /path/
 * @param poradi
 * @return
 */
String Soubor::getSouborName(const String &path, const uint16_t &poradi){
    //nazev souboru
    File dir = SD.open( path );
    //if(!playlistDir) Serial.println("!playlist");

    for (uint16_t i = 0; i < poradi; ++i) {
        File entry = dir.openNextFile();
        //if(!entry) Serial.println("!playlist-entry");
        entry.close();
    }

    {
        File entry = dir.openNextFile();
        String ret;
        if(!entry){
            //jsem patrne na konci playlistu
            //Serial.println("!playlist-entry-end");
            ret = "";
        }
        else {
            ret = entry.name();
            entry.close();
        }
        //ret.replace(".TXT", "");
        dir.close();
        return ret;
    }

}
