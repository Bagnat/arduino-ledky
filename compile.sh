hw=/mnt/c/Program\ Files\ \(x86\)/Arduino/hardware
arduino=$hw/arduino/avr
tool=$hw/tools/avr/bin
basic=$arduino/cores/arduino
libs=$arduino/libraries

# where you installed the Arduino app
ARDUINO_DIR=/mnt/c/Program\ Files\ \(x86\)/Arduino

# library sources
LIBRARY_DIR="${ARDUINO_DIR}hardware/arduino/avr/cores/arduino/"

# may need to change these
F_CPU=16000000
MCU=atmega328p
GENERAL_FLAGS="-x c++ -c -g -Os -Wall -ffunction-sections -fdata-sections -mmcu=${MCU} -DF_CPU=${F_CPU}L -MMD -DUSB_VID=null -DUSB_PID=null -DARDUINO=106"

"$tool/avr-g++.exe" \
$GENERAL_FLAGS \
-I"$ARDUINO_DIR/hardware/arduino/avr/cores/arduino" \
-I"$ARDUINO_DIR/hardware/arduino/variants/standard" \
-I"$ARDUINO_DIR/hardware/arduino/avr/libraries/SPI/src" \
ledky.ino
