cmake_minimum_required(VERSION 3.7)
project(ledky)

set(CMAKE_CXX_STANDARD 11)


set(SOURCE_FILES ledky.ino Pismeno.h Pismeno.cpp Interface.h Interface.cpp Ledky.h Ledky.cpp Soubor.cpp Soubor.h)
add_executable(ledky ${SOURCE_FILES})

set_target_properties(ledky PROPERTIES LINKER_LANGUAGE CXX)