#include <Arduino.h>
#include <SPI.h>

#include "Interface.h"
Interface* interface = NULL;

#include "Ledky.h"
Ledky* ledky = NULL;

void setup() {

    Serial.begin(9600);

    SPI.begin();
    SPI.beginTransaction(SPISettings(1600000000, LSBFIRST, SPI_MODE0));

    interface = new Interface();

    ledky = new Ledky(5);
    ledky->Clear();
    
    //načtení SD
    while (!SD.begin(SelectorSD)) {
        ledky->info("Neni SD");
    }

    //kontrola tlačítka
    while (interface->zmacknuti()){
        ledky->info("Neni tlacitko");
    }

    ledky->info("Start");

    //výběr módu
    if(Interface::zmacknuti()){
        //prehrati jednoho songu
        while(true){
            ledky->info("Vyber song");

            interface->vyberSong();

            {
                String songName = "/song/" + interface->getSongNameByNumber();
                Soubor pisen(songName);
                interface->delka_zmacknuti(true);
                interface->prehrajSong(pisen);
                interface->delka_zmacknuti(true);
            }
        }

    }else{
        ledky->info("Vyber playlist");
        interface->vyberPlaylist();
    }
}


void loop() {

    //ziskani songu, přehrání
    String songName = "/song/" + interface->getSongNameByPlaylist();

    if(songName == "/song/"){
      interface->delka_zmacknuti(true);
      ledky->info("konec - Vyber playlist");
      interface->vyberPlaylist();
      songName = "/song/" + interface->getSongNameByPlaylist();
    }
    Soubor pisen( songName );
    interface->delka_zmacknuti(true);
    interface->prehrajSong( pisen );

    interface->cislo_pisne++;
}
